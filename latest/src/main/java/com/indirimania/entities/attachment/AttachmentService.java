
package com.indirimania.entities.attachment;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indirimania.entities.PhpbbAttachment;
 
@Service
@Transactional
public class AttachmentService {
 
    @Autowired
    private AttachmentRepository repo;
     
    public List<PhpbbAttachment> listAll() {
        return repo.findAll();
    }
     
    public void save(PhpbbAttachment attach) {
        repo.save(attach);
    }
     
    public PhpbbAttachment get(int id) {
        return repo.findById(id).get();
    }
     
    public void delete(int id) {
        repo.deleteById(id);
    }
}