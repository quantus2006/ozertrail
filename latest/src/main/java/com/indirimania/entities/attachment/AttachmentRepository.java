package com.indirimania.entities.attachment; 
import org.springframework.data.jpa.repository.JpaRepository;

import com.indirimania.entities.PhpbbAttachment;
 
public interface AttachmentRepository extends JpaRepository<PhpbbAttachment, Integer> {
 
}