package com.indirimania.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the phpbb__posts database table.
 * 
 */
@Entity
@Table(name="phpbb_posts")
@NamedQuery(name="PhpbbPost.findAll", query="SELECT p FROM PhpbbPost p")
public class PhpbbPost implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="post_id")
	private int postId;

	@Column(name="bbcode_bitfield")
	private String bbcodeBitfield;

	@Column(name="bbcode_uid")
	private String bbcodeUid;

	@Column(name="enable_bbcode")
	private byte enableBbcode;

	@Column(name="enable_magic_url")
	private byte enableMagicUrl;

	@Column(name="enable_sig")
	private byte enableSig;

	@Column(name="enable_smilies")
	private byte enableSmilies;

	@Column(name="forum_id")
	private int forumId;

	@Column(name="icon_id")
	private int iconId;

	@Column(name="post_attachment")
	private byte postAttachment;

	@Column(name="post_checksum")
	private String postChecksum;

	@Column(name="post_delete_reason")
	private String postDeleteReason;

	@Column(name="post_delete_time")
	private int postDeleteTime;

	@Column(name="post_delete_user")
	private int postDeleteUser;

	@Column(name="post_edit_count")
	private int postEditCount;

	@Column(name="post_edit_locked")
	private byte postEditLocked;

	@Column(name="post_edit_reason")
	private String postEditReason;

	@Column(name="post_edit_time")
	private int postEditTime;

	@Column(name="post_edit_user")
	private int postEditUser;

	@Column(name="post_postcount")
	private byte postPostcount;

	@Column(name="post_reported")
	private byte postReported;

	@Column(name="post_subject")
	private String postSubject;

	@Lob
	@Column(name="post_text")
	private String postText;

	@Column(name="post_time")
	private int postTime;

	@Column(name="post_username")
	private String postUsername;

	@Column(name="post_visibility")
	private byte postVisibility;

	@Column(name="poster_id")
	private int posterId;

	@Column(name="poster_ip")
	private String posterIp;

	//bi-directional many-to-one association to PhpbbTopic
	@ManyToOne
	@JoinColumn(name="topic_id")
	private PhpbbTopic phpbbTopic;
	
	//bi-directional many-to-one association to PhpbbAttachment
	@OneToMany(mappedBy="phpbbPost")
	private List<PhpbbAttachment> phpbbAttachments;

	public PhpbbPost() {
	}

	public int getPostId() {
		return this.postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public String getBbcodeBitfield() {
		return this.bbcodeBitfield;
	}

	public void setBbcodeBitfield(String bbcodeBitfield) {
		this.bbcodeBitfield = bbcodeBitfield;
	}

	public String getBbcodeUid() {
		return this.bbcodeUid;
	}

	public void setBbcodeUid(String bbcodeUid) {
		this.bbcodeUid = bbcodeUid;
	}

	public byte getEnableBbcode() {
		return this.enableBbcode;
	}

	public void setEnableBbcode(byte enableBbcode) {
		this.enableBbcode = enableBbcode;
	}

	public byte getEnableMagicUrl() {
		return this.enableMagicUrl;
	}

	public void setEnableMagicUrl(byte enableMagicUrl) {
		this.enableMagicUrl = enableMagicUrl;
	}

	public byte getEnableSig() {
		return this.enableSig;
	}

	public void setEnableSig(byte enableSig) {
		this.enableSig = enableSig;
	}

	public byte getEnableSmilies() {
		return this.enableSmilies;
	}

	public void setEnableSmilies(byte enableSmilies) {
		this.enableSmilies = enableSmilies;
	}

	public int getForumId() {
		return this.forumId;
	}

	public void setForumId(int forumId) {
		this.forumId = forumId;
	}

	public int getIconId() {
		return this.iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	public byte getPostAttachment() {
		return this.postAttachment;
	}

	public void setPostAttachment(byte postAttachment) {
		this.postAttachment = postAttachment;
	}

	public String getPostChecksum() {
		return this.postChecksum;
	}

	public void setPostChecksum(String postChecksum) {
		this.postChecksum = postChecksum;
	}

	public String getPostDeleteReason() {
		return this.postDeleteReason;
	}

	public void setPostDeleteReason(String postDeleteReason) {
		this.postDeleteReason = postDeleteReason;
	}

	public int getPostDeleteTime() {
		return this.postDeleteTime;
	}

	public void setPostDeleteTime(int postDeleteTime) {
		this.postDeleteTime = postDeleteTime;
	}

	public int getPostDeleteUser() {
		return this.postDeleteUser;
	}

	public void setPostDeleteUser(int postDeleteUser) {
		this.postDeleteUser = postDeleteUser;
	}

	public int getPostEditCount() {
		return this.postEditCount;
	}

	public void setPostEditCount(int postEditCount) {
		this.postEditCount = postEditCount;
	}

	public byte getPostEditLocked() {
		return this.postEditLocked;
	}

	public void setPostEditLocked(byte postEditLocked) {
		this.postEditLocked = postEditLocked;
	}

	public String getPostEditReason() {
		return this.postEditReason;
	}

	public void setPostEditReason(String postEditReason) {
		this.postEditReason = postEditReason;
	}

	public int getPostEditTime() {
		return this.postEditTime;
	}

	public void setPostEditTime(int postEditTime) {
		this.postEditTime = postEditTime;
	}

	public int getPostEditUser() {
		return this.postEditUser;
	}

	public void setPostEditUser(int postEditUser) {
		this.postEditUser = postEditUser;
	}

	public byte getPostPostcount() {
		return this.postPostcount;
	}

	public void setPostPostcount(byte postPostcount) {
		this.postPostcount = postPostcount;
	}

	public byte getPostReported() {
		return this.postReported;
	}

	public void setPostReported(byte postReported) {
		this.postReported = postReported;
	}

	public String getPostSubject() {
		return this.postSubject;
	}

	public void setPostSubject(String postSubject) {
		this.postSubject = postSubject;
	}

	public String getPostText() {
		return this.postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

	public int getPostTime() {
		return this.postTime;
	}

	public void setPostTime(int postTime) {
		this.postTime = postTime;
	}

	public String getPostUsername() {
		return this.postUsername;
	}

	public void setPostUsername(String postUsername) {
		this.postUsername = postUsername;
	}

	public byte getPostVisibility() {
		return this.postVisibility;
	}

	public void setPostVisibility(byte postVisibility) {
		this.postVisibility = postVisibility;
	}

	public int getPosterId() {
		return this.posterId;
	}

	public void setPosterId(int posterId) {
		this.posterId = posterId;
	}

	public String getPosterIp() {
		return this.posterIp;
	}

	public void setPosterIp(String posterIp) {
		this.posterIp = posterIp;
	}

	public PhpbbTopic getPhpbbTopic() {
		return this.phpbbTopic;
	}

	public void setPhpbbTopic(PhpbbTopic phpbbTopic) {
		this.phpbbTopic = phpbbTopic;
	}

}