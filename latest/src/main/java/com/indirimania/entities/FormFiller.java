package com.indirimania.entities;

public class FormFiller {
	
	
	
	private String postTitle;
	
	public String getPostTitle() {
		return postTitle;
	}
	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}
	public int getOld_price() {
		return old_price;
	}
	public void setOld_price(int old_price) {
		this.old_price = old_price;
	}
	public int getNew_price() {
		return new_price;
	}
	public void setNew_price(int new_price) {
		this.new_price = new_price;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	private int old_price;
	private int new_price;
	private String detail;

}
