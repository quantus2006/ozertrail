package com.indirimania.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the phpbb_users database table.
 * 
 */
@Entity
@Table(name="phpbb_users")
@NamedQuery(name="PhpbbUser.findAll", query="SELECT p FROM PhpbbUser p")
public class PhpbbUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="user_actkey")
	private String userActkey;

	@Column(name="user_allow_massemail")
	private byte userAllowMassemail;

	@Column(name="user_allow_pm")
	private byte userAllowPm;

	@Column(name="user_allow_viewemail")
	private byte userAllowViewemail;

	@Column(name="user_allow_viewonline")
	private byte userAllowViewonline;

	@Column(name="user_avatar")
	private String userAvatar;

	@Column(name="user_avatar_height")
	private int userAvatarHeight;

	@Column(name="user_avatar_type")
	private String userAvatarType;

	@Column(name="user_avatar_width")
	private int userAvatarWidth;

	@Column(name="user_birthday")
	private String userBirthday;

	@Column(name="user_colour")
	private String userColour;

	@Column(name="user_dateformat")
	private String userDateformat;

	@Column(name="user_email")
	private String userEmail;

	@Column(name="user_email_hash")
	private BigInteger userEmailHash;

	@Column(name="user_emailtime")
	private int userEmailtime;

	@Column(name="user_form_salt")
	private String userFormSalt;

	@Column(name="user_full_folder")
	private int userFullFolder;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
	private int userId;

	@Column(name="user_inactive_reason")
	private byte userInactiveReason;

	@Column(name="user_inactive_time")
	private int userInactiveTime;

	@Column(name="user_ip")
	private String userIp;

	@Column(name="user_jabber")
	private String userJabber;

	@Column(name="user_lang")
	private String userLang;

	@Column(name="user_last_confirm_key")
	private String userLastConfirmKey;

	@Column(name="user_last_privmsg")
	private int userLastPrivmsg;

	@Column(name="user_last_search")
	private int userLastSearch;

	@Column(name="user_last_warning")
	private int userLastWarning;

	@Column(name="user_lastmark")
	private int userLastmark;

	@Column(name="user_lastpage")
	private String userLastpage;

	@Column(name="user_lastpost_time")
	private int userLastpostTime;

	@Column(name="user_lastvisit")
	private int userLastvisit;

	@Column(name="user_login_attempts")
	private byte userLoginAttempts;

	@Column(name="user_message_rules")
	private byte userMessageRules;

	@Column(name="user_new")
	private byte userNew;

	@Column(name="user_new_privmsg")
	private int userNewPrivmsg;

	@Column(name="user_newpasswd")
	private String userNewpasswd;

	@Column(name="user_notify")
	private byte userNotify;

	@Column(name="user_notify_pm")
	private byte userNotifyPm;

	@Column(name="user_notify_type")
	private byte userNotifyType;

	@Column(name="user_options")
	private int userOptions;

	@Column(name="user_passchg")
	private int userPasschg;

	@Column(name="user_password")
	private String userPassword;

	@Column(name="user_perm_from")
	private int userPermFrom;

	@Lob
	@Column(name="user_permissions")
	private String userPermissions;

	@Column(name="user_post_show_days")
	private int userPostShowDays;

	@Column(name="user_post_sortby_dir")
	private String userPostSortbyDir;

	@Column(name="user_post_sortby_type")
	private String userPostSortbyType;

	@Column(name="user_posts")
	private int userPosts;

	@Column(name="user_rank")
	private int userRank;

	@Column(name="user_regdate")
	private int userRegdate;

	@Column(name="user_reminded")
	private byte userReminded;

	@Column(name="user_reminded_time")
	private int userRemindedTime;

	@Lob
	@Column(name="user_sig")
	private String userSig;

	@Column(name="user_sig_bbcode_bitfield")
	private String userSigBbcodeBitfield;

	@Column(name="user_sig_bbcode_uid")
	private String userSigBbcodeUid;

	@Column(name="user_style")
	private int userStyle;

	@Column(name="user_timezone")
	private String userTimezone;

	@Column(name="user_topic_show_days")
	private int userTopicShowDays;

	@Column(name="user_topic_sortby_dir")
	private String userTopicSortbyDir;

	@Column(name="user_topic_sortby_type")
	private String userTopicSortbyType;

	@Column(name="user_type")
	private byte userType;

	@Column(name="user_unread_privmsg")
	private int userUnreadPrivmsg;

	@Column(name="user_warnings")
	private byte userWarnings;

	private String username;

	@Column(name="username_clean")
	private String usernameClean;

	public PhpbbUser() {
	}

	public String getUserActkey() {
		return this.userActkey;
	}

	public void setUserActkey(String userActkey) {
		this.userActkey = userActkey;
	}

	public byte getUserAllowMassemail() {
		return this.userAllowMassemail;
	}

	public void setUserAllowMassemail(byte userAllowMassemail) {
		this.userAllowMassemail = userAllowMassemail;
	}

	public byte getUserAllowPm() {
		return this.userAllowPm;
	}

	public void setUserAllowPm(byte userAllowPm) {
		this.userAllowPm = userAllowPm;
	}

	public byte getUserAllowViewemail() {
		return this.userAllowViewemail;
	}

	public void setUserAllowViewemail(byte userAllowViewemail) {
		this.userAllowViewemail = userAllowViewemail;
	}

	public byte getUserAllowViewonline() {
		return this.userAllowViewonline;
	}

	public void setUserAllowViewonline(byte userAllowViewonline) {
		this.userAllowViewonline = userAllowViewonline;
	}

	public String getUserAvatar() {
		return this.userAvatar;
	}

	public void setUserAvatar(String userAvatar) {
		this.userAvatar = userAvatar;
	}

	public int getUserAvatarHeight() {
		return this.userAvatarHeight;
	}

	public void setUserAvatarHeight(int userAvatarHeight) {
		this.userAvatarHeight = userAvatarHeight;
	}

	public String getUserAvatarType() {
		return this.userAvatarType;
	}

	public void setUserAvatarType(String userAvatarType) {
		this.userAvatarType = userAvatarType;
	}

	public int getUserAvatarWidth() {
		return this.userAvatarWidth;
	}

	public void setUserAvatarWidth(int userAvatarWidth) {
		this.userAvatarWidth = userAvatarWidth;
	}

	public String getUserBirthday() {
		return this.userBirthday;
	}

	public void setUserBirthday(String userBirthday) {
		this.userBirthday = userBirthday;
	}

	public String getUserColour() {
		return this.userColour;
	}

	public void setUserColour(String userColour) {
		this.userColour = userColour;
	}

	public String getUserDateformat() {
		return this.userDateformat;
	}

	public void setUserDateformat(String userDateformat) {
		this.userDateformat = userDateformat;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public BigInteger getUserEmailHash() {
		return this.userEmailHash;
	}

	public void setUserEmailHash(BigInteger userEmailHash) {
		this.userEmailHash = userEmailHash;
	}

	public int getUserEmailtime() {
		return this.userEmailtime;
	}

	public void setUserEmailtime(int userEmailtime) {
		this.userEmailtime = userEmailtime;
	}

	public String getUserFormSalt() {
		return this.userFormSalt;
	}

	public void setUserFormSalt(String userFormSalt) {
		this.userFormSalt = userFormSalt;
	}

	public int getUserFullFolder() {
		return this.userFullFolder;
	}

	public void setUserFullFolder(int userFullFolder) {
		this.userFullFolder = userFullFolder;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public byte getUserInactiveReason() {
		return this.userInactiveReason;
	}

	public void setUserInactiveReason(byte userInactiveReason) {
		this.userInactiveReason = userInactiveReason;
	}

	public int getUserInactiveTime() {
		return this.userInactiveTime;
	}

	public void setUserInactiveTime(int userInactiveTime) {
		this.userInactiveTime = userInactiveTime;
	}

	public String getUserIp() {
		return this.userIp;
	}

	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	public String getUserJabber() {
		return this.userJabber;
	}

	public void setUserJabber(String userJabber) {
		this.userJabber = userJabber;
	}

	public String getUserLang() {
		return this.userLang;
	}

	public void setUserLang(String userLang) {
		this.userLang = userLang;
	}

	public String getUserLastConfirmKey() {
		return this.userLastConfirmKey;
	}

	public void setUserLastConfirmKey(String userLastConfirmKey) {
		this.userLastConfirmKey = userLastConfirmKey;
	}

	public int getUserLastPrivmsg() {
		return this.userLastPrivmsg;
	}

	public void setUserLastPrivmsg(int userLastPrivmsg) {
		this.userLastPrivmsg = userLastPrivmsg;
	}

	public int getUserLastSearch() {
		return this.userLastSearch;
	}

	public void setUserLastSearch(int userLastSearch) {
		this.userLastSearch = userLastSearch;
	}

	public int getUserLastWarning() {
		return this.userLastWarning;
	}

	public void setUserLastWarning(int userLastWarning) {
		this.userLastWarning = userLastWarning;
	}

	public int getUserLastmark() {
		return this.userLastmark;
	}

	public void setUserLastmark(int userLastmark) {
		this.userLastmark = userLastmark;
	}

	public String getUserLastpage() {
		return this.userLastpage;
	}

	public void setUserLastpage(String userLastpage) {
		this.userLastpage = userLastpage;
	}

	public int getUserLastpostTime() {
		return this.userLastpostTime;
	}

	public void setUserLastpostTime(int userLastpostTime) {
		this.userLastpostTime = userLastpostTime;
	}

	public int getUserLastvisit() {
		return this.userLastvisit;
	}

	public void setUserLastvisit(int userLastvisit) {
		this.userLastvisit = userLastvisit;
	}

	public byte getUserLoginAttempts() {
		return this.userLoginAttempts;
	}

	public void setUserLoginAttempts(byte userLoginAttempts) {
		this.userLoginAttempts = userLoginAttempts;
	}

	public byte getUserMessageRules() {
		return this.userMessageRules;
	}

	public void setUserMessageRules(byte userMessageRules) {
		this.userMessageRules = userMessageRules;
	}

	public byte getUserNew() {
		return this.userNew;
	}

	public void setUserNew(byte userNew) {
		this.userNew = userNew;
	}

	public int getUserNewPrivmsg() {
		return this.userNewPrivmsg;
	}

	public void setUserNewPrivmsg(int userNewPrivmsg) {
		this.userNewPrivmsg = userNewPrivmsg;
	}

	public String getUserNewpasswd() {
		return this.userNewpasswd;
	}

	public void setUserNewpasswd(String userNewpasswd) {
		this.userNewpasswd = userNewpasswd;
	}

	public byte getUserNotify() {
		return this.userNotify;
	}

	public void setUserNotify(byte userNotify) {
		this.userNotify = userNotify;
	}

	public byte getUserNotifyPm() {
		return this.userNotifyPm;
	}

	public void setUserNotifyPm(byte userNotifyPm) {
		this.userNotifyPm = userNotifyPm;
	}

	public byte getUserNotifyType() {
		return this.userNotifyType;
	}

	public void setUserNotifyType(byte userNotifyType) {
		this.userNotifyType = userNotifyType;
	}

	public int getUserOptions() {
		return this.userOptions;
	}

	public void setUserOptions(int userOptions) {
		this.userOptions = userOptions;
	}

	public int getUserPasschg() {
		return this.userPasschg;
	}

	public void setUserPasschg(int userPasschg) {
		this.userPasschg = userPasschg;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public int getUserPermFrom() {
		return this.userPermFrom;
	}

	public void setUserPermFrom(int userPermFrom) {
		this.userPermFrom = userPermFrom;
	}

	public String getUserPermissions() {
		return this.userPermissions;
	}

	public void setUserPermissions(String userPermissions) {
		this.userPermissions = userPermissions;
	}

	public int getUserPostShowDays() {
		return this.userPostShowDays;
	}

	public void setUserPostShowDays(int userPostShowDays) {
		this.userPostShowDays = userPostShowDays;
	}

	public String getUserPostSortbyDir() {
		return this.userPostSortbyDir;
	}

	public void setUserPostSortbyDir(String userPostSortbyDir) {
		this.userPostSortbyDir = userPostSortbyDir;
	}

	public String getUserPostSortbyType() {
		return this.userPostSortbyType;
	}

	public void setUserPostSortbyType(String userPostSortbyType) {
		this.userPostSortbyType = userPostSortbyType;
	}

	public int getUserPosts() {
		return this.userPosts;
	}

	public void setUserPosts(int userPosts) {
		this.userPosts = userPosts;
	}

	public int getUserRank() {
		return this.userRank;
	}

	public void setUserRank(int userRank) {
		this.userRank = userRank;
	}

	public int getUserRegdate() {
		return this.userRegdate;
	}

	public void setUserRegdate(int userRegdate) {
		this.userRegdate = userRegdate;
	}

	public byte getUserReminded() {
		return this.userReminded;
	}

	public void setUserReminded(byte userReminded) {
		this.userReminded = userReminded;
	}

	public int getUserRemindedTime() {
		return this.userRemindedTime;
	}

	public void setUserRemindedTime(int userRemindedTime) {
		this.userRemindedTime = userRemindedTime;
	}

	public String getUserSig() {
		return this.userSig;
	}

	public void setUserSig(String userSig) {
		this.userSig = userSig;
	}

	public String getUserSigBbcodeBitfield() {
		return this.userSigBbcodeBitfield;
	}

	public void setUserSigBbcodeBitfield(String userSigBbcodeBitfield) {
		this.userSigBbcodeBitfield = userSigBbcodeBitfield;
	}

	public String getUserSigBbcodeUid() {
		return this.userSigBbcodeUid;
	}

	public void setUserSigBbcodeUid(String userSigBbcodeUid) {
		this.userSigBbcodeUid = userSigBbcodeUid;
	}

	public int getUserStyle() {
		return this.userStyle;
	}

	public void setUserStyle(int userStyle) {
		this.userStyle = userStyle;
	}

	public String getUserTimezone() {
		return this.userTimezone;
	}

	public void setUserTimezone(String userTimezone) {
		this.userTimezone = userTimezone;
	}

	public int getUserTopicShowDays() {
		return this.userTopicShowDays;
	}

	public void setUserTopicShowDays(int userTopicShowDays) {
		this.userTopicShowDays = userTopicShowDays;
	}

	public String getUserTopicSortbyDir() {
		return this.userTopicSortbyDir;
	}

	public void setUserTopicSortbyDir(String userTopicSortbyDir) {
		this.userTopicSortbyDir = userTopicSortbyDir;
	}

	public String getUserTopicSortbyType() {
		return this.userTopicSortbyType;
	}

	public void setUserTopicSortbyType(String userTopicSortbyType) {
		this.userTopicSortbyType = userTopicSortbyType;
	}

	public byte getUserType() {
		return this.userType;
	}

	public void setUserType(byte userType) {
		this.userType = userType;
	}

	public int getUserUnreadPrivmsg() {
		return this.userUnreadPrivmsg;
	}

	public void setUserUnreadPrivmsg(int userUnreadPrivmsg) {
		this.userUnreadPrivmsg = userUnreadPrivmsg;
	}

	public byte getUserWarnings() {
		return this.userWarnings;
	}

	public void setUserWarnings(byte userWarnings) {
		this.userWarnings = userWarnings;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsernameClean() {
		return this.usernameClean;
	}

	public void setUsernameClean(String usernameClean) {
		this.usernameClean = usernameClean;
	}

}