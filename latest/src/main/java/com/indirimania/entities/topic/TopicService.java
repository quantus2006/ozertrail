
package com.indirimania.entities.topic;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indirimania.entities.PhpbbTopic;
 
@Service
@Transactional
public class TopicService {
 
    @Autowired
    private TopicRepository repo;
     
    public List<PhpbbTopic> listAll() {
        return repo.findAll();
    }
     
    public void save(PhpbbTopic topic) {
        repo.save(topic);
    }
     
    public PhpbbTopic get(int id) {
        return repo.findById(id).get();
    }
     
    public void delete(int id) {
        repo.deleteById(id);
    }
}