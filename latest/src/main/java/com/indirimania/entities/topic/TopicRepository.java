package com.indirimania.entities.topic; 
import org.springframework.data.jpa.repository.JpaRepository;

import com.indirimania.entities.PhpbbTopic;
 
public interface TopicRepository extends JpaRepository<PhpbbTopic, Integer> {
 
}