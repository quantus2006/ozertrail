package com.indirimania.entities;

import java.io.Serializable;
import javax.persistence.*;
  
import java.util.List;


/**
 * The persistent class for the phpbb__topics database table.
 * 
 */
@Entity
@Table(name="phpbb_topics")
@NamedQuery(name="PhpbbTopic.findAll", query="SELECT p FROM PhpbbTopic p")
public class PhpbbTopic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="topic_id")
	private int topicId;

	@Column(name="icon_id")
	private int iconId;

	@Column(name="poll_last_vote")
	private int pollLastVote;

	@Column(name="poll_length")
	private int pollLength;

	@Column(name="poll_max_options")
	private byte pollMaxOptions;

	@Column(name="poll_start")
	private int pollStart;

	@Column(name="poll_title")
	private String pollTitle;

	@Column(name="poll_vote_change")
	private String pollVoteChange;

	@Column(name="topic_attachment")
	private byte topicAttachment;

	@Column(name="topic_bumped")
	private String topicBumped;

	@Column(name="topic_bumper")
	private int topicBumper;

	@Column(name="topic_delete_reason")
	private String topicDeleteReason;

	@Column(name="topic_delete_time")
	private int topicDeleteTime;

	@Column(name="topic_delete_user")
	private int topicDeleteUser;

	@Column(name="topic_first_post_id")
	private int topicFirstPostId;

	@Column(name="topic_first_poster_colour")
	private String topicFirstPosterColour;

	@Column(name="topic_first_poster_name")
	private String topicFirstPosterName;

	@Column(name="topic_last_post_id")
	private int topicLastPostId;

	@Column(name="topic_last_post_subject")
	private String topicLastPostSubject;

	@Column(name="topic_last_post_time")
	private int topicLastPostTime;

	@Column(name="topic_last_poster_colour")
	private String topicLastPosterColour;

	@Column(name="topic_last_poster_id")
	private int topicLastPosterId;

	@Column(name="topic_last_poster_name")
	private String topicLastPosterName;

	@Column(name="topic_last_view_time")
	private int topicLastViewTime;

	@Column(name="topic_moved_id")
	private int topicMovedId;

	@Column(name="topic_poster")
	private int topicPoster;

	@Column(name="topic_posts_approved")
	private int topicPostsApproved;

	@Column(name="topic_posts_softdeleted")
	private int topicPostsSoftdeleted;

	@Column(name="topic_posts_unapproved")
	private int topicPostsUnapproved;

	@Column(name="topic_reported")
	private String topicReported;

	@Column(name="topic_status")
	private byte topicStatus;

	@Column(name="topic_time")
	private int topicTime;

	@Column(name="topic_time_limit")
	private int topicTimeLimit;

	@Column(name="topic_title")
	private String topicTitle;

	@Column(name="topic_type")
	private byte topicType;

	@Column(name="topic_views")
	private int topicViews;

	@Column(name="topic_visibility")
	private byte topicVisibility;
	
	
	@Column(name="new_price")
	private int newPrice;
	
	
	@Column(name="old_price")
	private int oldPrice;
	
	
	@Column(name="topic_url")
	private String topicUrl;

	public String getTopicUrl() {
		return topicUrl;
	}

	public void setTopicUrl(String topicUrl) {
		this.topicUrl = topicUrl;
	}

	//bi-directional many-to-one association to PhpbbForum
	@ManyToOne
	@JoinColumn(name="forum_id")
	private PhpbbForum phpbbForum;

	//bi-directional many-to-one association to PhpbbPost
	@OneToMany(mappedBy="phpbbTopic")
	private List<PhpbbPost> phpbbPosts;
	
	
	//bi-directional many-to-one association to PhpbbAttachment
	@OneToMany(mappedBy="phpbbTopic")
	private List<PhpbbAttachment> phpbbAttachments;

	public byte getTopicAttachment() {
		return topicAttachment;
	}

	public void setTopicAttachment(byte topicAttachment) {
		this.topicAttachment = topicAttachment;
	}

	public int getNewPrice() {
		return newPrice;
	}

	public void setNewPrice(int newPrice) {
		this.newPrice = newPrice;
	}

	public int getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(int oldPrice) {
		this.oldPrice = oldPrice;
	}

	public PhpbbTopic() {
	}

	public int getTopicId() {
		return this.topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public int getIconId() {
		return this.iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	public int getPollLastVote() {
		return this.pollLastVote;
	}

	public void setPollLastVote(int pollLastVote) {
		this.pollLastVote = pollLastVote;
	}

	public int getPollLength() {
		return this.pollLength;
	}

	public void setPollLength(int pollLength) {
		this.pollLength = pollLength;
	}

	public byte getPollMaxOptions() {
		return this.pollMaxOptions;
	}

	public void setPollMaxOptions(byte pollMaxOptions) {
		this.pollMaxOptions = pollMaxOptions;
	}

	public int getPollStart() {
		return this.pollStart;
	}

	public void setPollStart(int pollStart) {
		this.pollStart = pollStart;
	}

	public String getPollTitle() {
		return this.pollTitle;
	}

	public void setPollTitle(String pollTitle) {
		this.pollTitle = pollTitle;
	}

	public String getPollVoteChange() {
		return this.pollVoteChange;
	}

	public void setPollVoteChange(String pollVoteChange) {
		this.pollVoteChange = pollVoteChange;
	}

	public List<PhpbbAttachment> getPhpbbAttachments() {
		return this.phpbbAttachments;
	}

	public void setPhpbbAttachments(List<PhpbbAttachment> phpbbAttachments) {
		this.phpbbAttachments = phpbbAttachments;
	}

	public PhpbbAttachment addPhpbbAttachment(PhpbbAttachment phpbbAttachment) {
		getPhpbbAttachments().add(phpbbAttachment);
		phpbbAttachment.setPhpbbTopic(this);

		return phpbbAttachment;
	}

	public PhpbbAttachment removePhpbbAttachment(PhpbbAttachment phpbbAttachment) {
		getPhpbbAttachments().remove(phpbbAttachment);
		phpbbAttachment.setPhpbbTopic(null);

		return phpbbAttachment;
	}

	public String getTopicBumped() {
		return this.topicBumped;
	}

	public void setTopicBumped(String topicBumped) {
		this.topicBumped = topicBumped;
	}

	public int getTopicBumper() {
		return this.topicBumper;
	}

	public void setTopicBumper(int topicBumper) {
		this.topicBumper = topicBumper;
	}

	public String getTopicDeleteReason() {
		return this.topicDeleteReason;
	}

	public void setTopicDeleteReason(String topicDeleteReason) {
		this.topicDeleteReason = topicDeleteReason;
	}

	public int getTopicDeleteTime() {
		return this.topicDeleteTime;
	}

	public void setTopicDeleteTime(int topicDeleteTime) {
		this.topicDeleteTime = topicDeleteTime;
	}

	public int getTopicDeleteUser() {
		return this.topicDeleteUser;
	}

	public void setTopicDeleteUser(int topicDeleteUser) {
		this.topicDeleteUser = topicDeleteUser;
	}

	public int getTopicFirstPostId() {
		return this.topicFirstPostId;
	}

	public void setTopicFirstPostId(int topicFirstPostId) {
		this.topicFirstPostId = topicFirstPostId;
	}

	public String getTopicFirstPosterColour() {
		return this.topicFirstPosterColour;
	}

	public void setTopicFirstPosterColour(String topicFirstPosterColour) {
		this.topicFirstPosterColour = topicFirstPosterColour;
	}

	public String getTopicFirstPosterName() {
		return this.topicFirstPosterName;
	}

	public void setTopicFirstPosterName(String topicFirstPosterName) {
		this.topicFirstPosterName = topicFirstPosterName;
	}

	public int getTopicLastPostId() {
		return this.topicLastPostId;
	}

	public void setTopicLastPostId(int topicLastPostId) {
		this.topicLastPostId = topicLastPostId;
	}

	public String getTopicLastPostSubject() {
		return this.topicLastPostSubject;
	}

	public void setTopicLastPostSubject(String topicLastPostSubject) {
		this.topicLastPostSubject = topicLastPostSubject;
	}

	public int getTopicLastPostTime() {
		return this.topicLastPostTime;
	}

	public void setTopicLastPostTime(int topicLastPostTime) {
		this.topicLastPostTime = topicLastPostTime;
	}

	public String getTopicLastPosterColour() {
		return this.topicLastPosterColour;
	}

	public void setTopicLastPosterColour(String topicLastPosterColour) {
		this.topicLastPosterColour = topicLastPosterColour;
	}

	public int getTopicLastPosterId() {
		return this.topicLastPosterId;
	}

	public void setTopicLastPosterId(int topicLastPosterId) {
		this.topicLastPosterId = topicLastPosterId;
	}

	public String getTopicLastPosterName() {
		return this.topicLastPosterName;
	}

	public void setTopicLastPosterName(String topicLastPosterName) {
		this.topicLastPosterName = topicLastPosterName;
	}

	public int getTopicLastViewTime() {
		return this.topicLastViewTime;
	}

	public void setTopicLastViewTime(int topicLastViewTime) {
		this.topicLastViewTime = topicLastViewTime;
	}

	public int getTopicMovedId() {
		return this.topicMovedId;
	}

	public void setTopicMovedId(int topicMovedId) {
		this.topicMovedId = topicMovedId;
	}

	public int getTopicPoster() {
		return this.topicPoster;
	}

	public void setTopicPoster(int topicPoster) {
		this.topicPoster = topicPoster;
	}

	public int getTopicPostsApproved() {
		return this.topicPostsApproved;
	}

	public void setTopicPostsApproved(int topicPostsApproved) {
		this.topicPostsApproved = topicPostsApproved;
	}

	public int getTopicPostsSoftdeleted() {
		return this.topicPostsSoftdeleted;
	}

	public void setTopicPostsSoftdeleted(int topicPostsSoftdeleted) {
		this.topicPostsSoftdeleted = topicPostsSoftdeleted;
	}

	public int getTopicPostsUnapproved() {
		return this.topicPostsUnapproved;
	}

	public void setTopicPostsUnapproved(int topicPostsUnapproved) {
		this.topicPostsUnapproved = topicPostsUnapproved;
	}

	public String getTopicReported() {
		return this.topicReported;
	}

	public void setTopicReported(String topicReported) {
		this.topicReported = topicReported;
	}

	public byte getTopicStatus() {
		return this.topicStatus;
	}

	public void setTopicStatus(byte topicStatus) {
		this.topicStatus = topicStatus;
	}

	public int getTopicTime() {
		return this.topicTime;
	}

	public void setTopicTime(int topicTime) {
		this.topicTime = topicTime;
	}

	public int getTopicTimeLimit() {
		return this.topicTimeLimit;
	}

	public void setTopicTimeLimit(int topicTimeLimit) {
		this.topicTimeLimit = topicTimeLimit;
	}

	public String getTopicTitle() {
		return this.topicTitle;
	}

	public void setTopicTitle(String topicTitle) {
		this.topicTitle = topicTitle;
	}

	public byte getTopicType() {
		return this.topicType;
	}

	public void setTopicType(byte topicType) {
		this.topicType = topicType;
	}

	public int getTopicViews() {
		return this.topicViews;
	}

	public void setTopicViews(int topicViews) {
		this.topicViews = topicViews;
	}

	public byte getTopicVisibility() {
		return this.topicVisibility;
	}

	public void setTopicVisibility(byte topicVisibility) {
		this.topicVisibility = topicVisibility;
	}

	public PhpbbForum getPhpbbForum() {
		return this.phpbbForum;
	}

	public void setPhpbbForum(PhpbbForum phpbbForum) {
		this.phpbbForum = phpbbForum;
	}

	public List<PhpbbPost> getPhpbbPosts() {
		return this.phpbbPosts;
	}

	public void setPhpbbPosts(List<PhpbbPost> phpbbPosts) {
		this.phpbbPosts = phpbbPosts;
	}

	public PhpbbPost addPhpbbPost(PhpbbPost phpbbPost) {
		getPhpbbPosts().add(phpbbPost);
		phpbbPost.setPhpbbTopic(this);

		return phpbbPost;
	}

	public PhpbbPost removePhpbbPost(PhpbbPost phpbbPost) {
		getPhpbbPosts().remove(phpbbPost);
		phpbbPost.setPhpbbTopic(null);

		return phpbbPost;
	}

}