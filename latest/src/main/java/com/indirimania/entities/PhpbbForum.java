package com.indirimania.entities;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the phpbb__forums database table.
 * 
 */
@Entity
@Table(name="phpbb_forums")
@NamedQuery(name="PhpbbForum.findAll", query="SELECT p FROM PhpbbForum p")
public class PhpbbForum implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="forum_id")
	private int forumId;

	@Column(name="display_on_index")
	private String displayOnIndex;

	@Column(name="display_subforum_list")
	private String displaySubforumList;

	@Column(name="enable_icons")
	private String enableIcons;

	@Column(name="enable_indexing")
	private String enableIndexing;

	@Column(name="enable_prune")
	private String enablePrune;

	@Column(name="enable_shadow_prune")
	private String enableShadowPrune;

	@Column(name="forum_desc")
	private String forumDesc;

	@Column(name="forum_desc_bitfield")
	private String forumDescBitfield;

	@Column(name="forum_desc_options")
	private int forumDescOptions;

	@Column(name="forum_desc_uid")
	private String forumDescUid;

	@Column(name="forum_flags")
	private byte forumFlags;

	@Column(name="forum_image")
	private String forumImage;

	@Column(name="forum_last_post_id")
	private int forumLastPostId;

	@Column(name="forum_last_post_subject")
	private String forumLastPostSubject;

	@Column(name="forum_last_post_time")
	private int forumLastPostTime;

	@Column(name="forum_last_poster_colour")
	private String forumLastPosterColour;

	@Column(name="forum_last_poster_id")
	private int forumLastPosterId;

	@Column(name="forum_last_poster_name")
	private String forumLastPosterName;

	@Column(name="forum_link")
	private String forumLink;

	@Column(name="forum_name")
	private String forumName;

	@Column(name="forum_options")
	private int forumOptions;

	@Column(name="forum_parents")
	private String forumParents;

	@Column(name="forum_password")
	private String forumPassword;

	@Column(name="forum_posts_approved")
	private int forumPostsApproved;

	@Column(name="forum_posts_softdeleted")
	private int forumPostsSoftdeleted;

	@Column(name="forum_posts_unapproved")
	private int forumPostsUnapproved;

	@Column(name="forum_rules")
	private String forumRules;

	@Column(name="forum_rules_bitfield")
	private String forumRulesBitfield;

	@Column(name="forum_rules_link")
	private String forumRulesLink;

	@Column(name="forum_rules_options")
	private int forumRulesOptions;

	@Column(name="forum_rules_uid")
	private String forumRulesUid;

	@Column(name="forum_status")
	private byte forumStatus;

	@Column(name="forum_style")
	private int forumStyle;

	@Column(name="forum_topics_approved")
	private int forumTopicsApproved;

	@Column(name="forum_topics_per_page")
	private int forumTopicsPerPage;

	@Column(name="forum_topics_softdeleted")
	private int forumTopicsSoftdeleted;

	@Column(name="forum_topics_unapproved")
	private int forumTopicsUnapproved;

	@Column(name="forum_type")
	private byte forumType;

	@Column(name="left_id")
	private int leftId;

	@Column(name="parent_id")
	private int parentId;

	@Column(name="prune_days")
	private int pruneDays;

	@Column(name="prune_freq")
	private int pruneFreq;

	@Column(name="prune_next")
	private int pruneNext;

	@Column(name="prune_shadow_days")
	private int pruneShadowDays;

	@Column(name="prune_shadow_freq")
	private int pruneShadowFreq;

	@Column(name="prune_shadow_next")
	private int pruneShadowNext;

	@Column(name="prune_viewed")
	private int pruneViewed;

	@Column(name="right_id")
	private int rightId;

	//bi-directional many-to-one association to PhpbbTopic
	@OneToMany(mappedBy="phpbbForum")
	private List<PhpbbTopic> phpbbTopics;

	public PhpbbForum() {
	}

	public int getForumId() {
		return this.forumId;
	}

	public void setForumId(int forumId) {
		this.forumId = forumId;
	}

	public String getDisplayOnIndex() {
		return this.displayOnIndex;
	}

	public void setDisplayOnIndex(String displayOnIndex) {
		this.displayOnIndex = displayOnIndex;
	}

	public String getDisplaySubforumList() {
		return this.displaySubforumList;
	}

	public void setDisplaySubforumList(String displaySubforumList) {
		this.displaySubforumList = displaySubforumList;
	}

	public String getEnableIcons() {
		return this.enableIcons;
	}

	public void setEnableIcons(String enableIcons) {
		this.enableIcons = enableIcons;
	}

	public String getEnableIndexing() {
		return this.enableIndexing;
	}

	public void setEnableIndexing(String enableIndexing) {
		this.enableIndexing = enableIndexing;
	}

	public String getEnablePrune() {
		return this.enablePrune;
	}

	public void setEnablePrune(String enablePrune) {
		this.enablePrune = enablePrune;
	}

	public String getEnableShadowPrune() {
		return this.enableShadowPrune;
	}

	public void setEnableShadowPrune(String enableShadowPrune) {
		this.enableShadowPrune = enableShadowPrune;
	}

	public String getForumDesc() {
		return this.forumDesc;
	}

	public void setForumDesc(String forumDesc) {
		this.forumDesc = forumDesc;
	}

	public String getForumDescBitfield() {
		return this.forumDescBitfield;
	}

	public void setForumDescBitfield(String forumDescBitfield) {
		this.forumDescBitfield = forumDescBitfield;
	}

	public int getForumDescOptions() {
		return this.forumDescOptions;
	}

	public void setForumDescOptions(int forumDescOptions) {
		this.forumDescOptions = forumDescOptions;
	}

	public String getForumDescUid() {
		return this.forumDescUid;
	}

	public void setForumDescUid(String forumDescUid) {
		this.forumDescUid = forumDescUid;
	}

	public byte getForumFlags() {
		return this.forumFlags;
	}

	public void setForumFlags(byte forumFlags) {
		this.forumFlags = forumFlags;
	}

	public String getForumImage() {
		return this.forumImage;
	}

	public void setForumImage(String forumImage) {
		this.forumImage = forumImage;
	}

	public int getForumLastPostId() {
		return this.forumLastPostId;
	}

	public void setForumLastPostId(int forumLastPostId) {
		this.forumLastPostId = forumLastPostId;
	}

	public String getForumLastPostSubject() {
		return this.forumLastPostSubject;
	}

	public void setForumLastPostSubject(String forumLastPostSubject) {
		this.forumLastPostSubject = forumLastPostSubject;
	}

	public int getForumLastPostTime() {
		return this.forumLastPostTime;
	}

	public void setForumLastPostTime(int forumLastPostTime) {
		this.forumLastPostTime = forumLastPostTime;
	}

	public String getForumLastPosterColour() {
		return this.forumLastPosterColour;
	}

	public void setForumLastPosterColour(String forumLastPosterColour) {
		this.forumLastPosterColour = forumLastPosterColour;
	}

	public int getForumLastPosterId() {
		return this.forumLastPosterId;
	}

	public void setForumLastPosterId(int forumLastPosterId) {
		this.forumLastPosterId = forumLastPosterId;
	}

	public String getForumLastPosterName() {
		return this.forumLastPosterName;
	}

	public void setForumLastPosterName(String forumLastPosterName) {
		this.forumLastPosterName = forumLastPosterName;
	}

	public String getForumLink() {
		return this.forumLink;
	}

	public void setForumLink(String forumLink) {
		this.forumLink = forumLink;
	}

	public String getForumName() {
		return this.forumName;
	}

	public void setForumName(String forumName) {
		this.forumName = forumName;
	}

	public int getForumOptions() {
		return this.forumOptions;
	}

	public void setForumOptions(int forumOptions) {
		this.forumOptions = forumOptions;
	}

	public String getForumParents() {
		return this.forumParents;
	}

	public void setForumParents(String forumParents) {
		this.forumParents = forumParents;
	}

	public String getForumPassword() {
		return this.forumPassword;
	}

	public void setForumPassword(String forumPassword) {
		this.forumPassword = forumPassword;
	}

	public int getForumPostsApproved() {
		return this.forumPostsApproved;
	}

	public void setForumPostsApproved(int forumPostsApproved) {
		this.forumPostsApproved = forumPostsApproved;
	}

	public int getForumPostsSoftdeleted() {
		return this.forumPostsSoftdeleted;
	}

	public void setForumPostsSoftdeleted(int forumPostsSoftdeleted) {
		this.forumPostsSoftdeleted = forumPostsSoftdeleted;
	}

	public int getForumPostsUnapproved() {
		return this.forumPostsUnapproved;
	}

	public void setForumPostsUnapproved(int forumPostsUnapproved) {
		this.forumPostsUnapproved = forumPostsUnapproved;
	}

	public String getForumRules() {
		return this.forumRules;
	}

	public void setForumRules(String forumRules) {
		this.forumRules = forumRules;
	}

	public String getForumRulesBitfield() {
		return this.forumRulesBitfield;
	}

	public void setForumRulesBitfield(String forumRulesBitfield) {
		this.forumRulesBitfield = forumRulesBitfield;
	}

	public String getForumRulesLink() {
		return this.forumRulesLink;
	}

	public void setForumRulesLink(String forumRulesLink) {
		this.forumRulesLink = forumRulesLink;
	}

	public int getForumRulesOptions() {
		return this.forumRulesOptions;
	}

	public void setForumRulesOptions(int forumRulesOptions) {
		this.forumRulesOptions = forumRulesOptions;
	}

	public String getForumRulesUid() {
		return this.forumRulesUid;
	}

	public void setForumRulesUid(String forumRulesUid) {
		this.forumRulesUid = forumRulesUid;
	}

	public byte getForumStatus() {
		return this.forumStatus;
	}

	public void setForumStatus(byte forumStatus) {
		this.forumStatus = forumStatus;
	}

	public int getForumStyle() {
		return this.forumStyle;
	}

	public void setForumStyle(int forumStyle) {
		this.forumStyle = forumStyle;
	}

	public int getForumTopicsApproved() {
		return this.forumTopicsApproved;
	}

	public void setForumTopicsApproved(int forumTopicsApproved) {
		this.forumTopicsApproved = forumTopicsApproved;
	}

	public int getForumTopicsPerPage() {
		return this.forumTopicsPerPage;
	}

	public void setForumTopicsPerPage(int forumTopicsPerPage) {
		this.forumTopicsPerPage = forumTopicsPerPage;
	}

	public int getForumTopicsSoftdeleted() {
		return this.forumTopicsSoftdeleted;
	}

	public void setForumTopicsSoftdeleted(int forumTopicsSoftdeleted) {
		this.forumTopicsSoftdeleted = forumTopicsSoftdeleted;
	}

	public int getForumTopicsUnapproved() {
		return this.forumTopicsUnapproved;
	}

	public void setForumTopicsUnapproved(int forumTopicsUnapproved) {
		this.forumTopicsUnapproved = forumTopicsUnapproved;
	}

	public byte getForumType() {
		return this.forumType;
	}

	public void setForumType(byte forumType) {
		this.forumType = forumType;
	}

	public int getLeftId() {
		return this.leftId;
	}

	public void setLeftId(int leftId) {
		this.leftId = leftId;
	}

	public int getParentId() {
		return this.parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public int getPruneDays() {
		return this.pruneDays;
	}

	public void setPruneDays(int pruneDays) {
		this.pruneDays = pruneDays;
	}

	public int getPruneFreq() {
		return this.pruneFreq;
	}

	public void setPruneFreq(int pruneFreq) {
		this.pruneFreq = pruneFreq;
	}

	public int getPruneNext() {
		return this.pruneNext;
	}

	public void setPruneNext(int pruneNext) {
		this.pruneNext = pruneNext;
	}

	public int getPruneShadowDays() {
		return this.pruneShadowDays;
	}

	public void setPruneShadowDays(int pruneShadowDays) {
		this.pruneShadowDays = pruneShadowDays;
	}

	public int getPruneShadowFreq() {
		return this.pruneShadowFreq;
	}

	public void setPruneShadowFreq(int pruneShadowFreq) {
		this.pruneShadowFreq = pruneShadowFreq;
	}

	public int getPruneShadowNext() {
		return this.pruneShadowNext;
	}

	public void setPruneShadowNext(int pruneShadowNext) {
		this.pruneShadowNext = pruneShadowNext;
	}

	public int getPruneViewed() {
		return this.pruneViewed;
	}

	public void setPruneViewed(int pruneViewed) {
		this.pruneViewed = pruneViewed;
	}

	public int getRightId() {
		return this.rightId;
	}

	public void setRightId(int rightId) {
		this.rightId = rightId;
	}

	public List<PhpbbTopic> getPhpbbTopics() {
		return this.phpbbTopics;
	}

	public void setPhpbbTopics(List<PhpbbTopic> phpbbTopics) {
		this.phpbbTopics = phpbbTopics;
	}

	public PhpbbTopic addPhpbbTopic(PhpbbTopic phpbbTopic) {
		getPhpbbTopics().add(phpbbTopic);
		phpbbTopic.setPhpbbForum(this);

		return phpbbTopic;
	}

	public PhpbbTopic removePhpbbTopic(PhpbbTopic phpbbTopic) {
		getPhpbbTopics().remove(phpbbTopic);
		phpbbTopic.setPhpbbForum(null);

		return phpbbTopic;
	}

}