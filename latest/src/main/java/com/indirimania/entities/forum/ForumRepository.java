package com.indirimania.entities.forum; 
import org.springframework.data.jpa.repository.JpaRepository;

import com.indirimania.entities.PhpbbForum;
 
public interface ForumRepository extends JpaRepository<PhpbbForum, Integer> {
 
}