
package com.indirimania.entities.forum;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indirimania.entities.PhpbbForum;
 
@Service
@Transactional
public class ForumService {
 
    @Autowired
    private ForumRepository repo;
     
    public List<PhpbbForum> listAll() {
        return repo.findAll();
    }
     
    public void save(PhpbbForum topic) {
        repo.save(topic);
    }
     
    public PhpbbForum get(int id) {
        return repo.findById(id).get();
    }
     
    public void delete(int id) {
        repo.deleteById(id);
    }
}