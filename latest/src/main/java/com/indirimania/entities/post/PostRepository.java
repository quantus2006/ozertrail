package com.indirimania.entities.post; 
import org.springframework.data.jpa.repository.JpaRepository;

import com.indirimania.entities.PhpbbPost;
 
public interface PostRepository extends JpaRepository<PhpbbPost, Integer> {
 
}