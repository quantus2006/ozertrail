
package com.indirimania.entities.post;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indirimania.entities.PhpbbPost;
 
@Service
@Transactional
public class PostService {
 
    @Autowired
    private PostRepository repo;
     
    public List<PhpbbPost> listAll() {
        return repo.findAll();
    }
     
    public void save(PhpbbPost post) {
        repo.save(post);
    }
     
    public PhpbbPost get(int id) {
        return repo.findById(id).get();
    }
     
    public void delete(int id) {
        repo.deleteById(id);
    }
}