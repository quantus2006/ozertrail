package com.indirimania.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the phpbb__attachments database table.
 * 
 */
@Entity
@Table(name="phpbb_attachments")
@NamedQuery(name="PhpbbAttachment.findAll", query="SELECT p FROM PhpbbAttachment p")
public class PhpbbAttachment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="attach_id")
	private int attachid;

	@Lob
	@Column(name="attach_comment")
	private String attachComment;

	@Column(name="download_count")
	private int downloadCount;

	private String extension;

	private int filesize;

	private int filetime;

	@Column(name="in_message")
	private byte inMessage;

	@Column(name="is_orphan")
	private byte isOrphan;

	private String mimetype;

	@Column(name="physical_filename")
	private String physicalFilename;

	//@Column(name="post_msg_id")
	//private int postMsgId;

	@Column(name="poster_id")
	private int posterId;

	@Column(name="real_filename")
	private String realFilename;

	private byte thumbnail;

	//bi-directional many-to-one association to PhpbbTopic
	@ManyToOne
	@JoinColumn(name="topic_id", referencedColumnName="topic_id")
	private PhpbbTopic phpbbTopic;
	
	@ManyToOne
	//@JoinColumn(name="post_id", referencedColumnName="post_id")
	@JoinColumn(name="post_msg_id")
	private PhpbbPost phpbbPost;

	public int getAttachid() {
		return attachid;
	}

	public void setAttachid(int attachid) {
		this.attachid = attachid;
	}

	public PhpbbAttachment() {
	}

	public int getAttachId() {
		return this.attachid;
	}

	public void setAttachId(int attachId) {
		this.attachid = attachId;
	}

	public String getAttachComment() {
		return this.attachComment;
	}

	public PhpbbPost getPhpbbPost() {
		return phpbbPost;
	}

	public void setPhpbbPost(PhpbbPost phpbbPost) {
		this.phpbbPost = phpbbPost;
	}

	public void setAttachComment(String attachComment) {
		this.attachComment = attachComment;
	}

	public int getDownloadCount() {
		return this.downloadCount;
	}

	public void setDownloadCount(int downloadCount) {
		this.downloadCount = downloadCount;
	}

	public String getExtension() {
		return this.extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public int getFilesize() {
		return this.filesize;
	}

	public void setFilesize(int filesize) {
		this.filesize = filesize;
	}

	public int getFiletime() {
		return this.filetime;
	}

	public void setFiletime(int filetime) {
		this.filetime = filetime;
	}

	public byte getInMessage() {
		return this.inMessage;
	}

	public void setInMessage(byte inMessage) {
		this.inMessage = inMessage;
	}

	public byte getIsOrphan() {
		return this.isOrphan;
	}

	public void setIsOrphan(byte isOrphan) {
		this.isOrphan = isOrphan;
	}

	public String getMimetype() {
		return this.mimetype;
	}

	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	public String getPhysicalFilename() {
		return this.physicalFilename;
	}

	public void setPhysicalFilename(String physicalFilename) {
		this.physicalFilename = physicalFilename;
	}



	public int getPosterId() {
		return this.posterId;
	}

	public void setPosterId(int posterId) {
		this.posterId = posterId;
	}

	public String getRealFilename() {
		return this.realFilename;
	}

	public void setRealFilename(String realFilename) {
		this.realFilename = realFilename;
	}

	public byte getThumbnail() {
		return this.thumbnail;
	}

	public void setThumbnail(byte thumbnail) {
		this.thumbnail = thumbnail;
	}

	public PhpbbTopic getPhpbbTopic() {
		return this.phpbbTopic;
	}

	public void setPhpbbTopic(PhpbbTopic phpbbTopic) {
		this.phpbbTopic = phpbbTopic;
	}

}