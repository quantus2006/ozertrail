package com.indirimania.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.indirimania"})
@EntityScan("com.indirimania.entities")
@EnableJpaRepositories({"com.indirimania.entities.topic","com.indirimania.entities.forum","com.indirimania.entities.post","com.indirimania.entities.attachment"})
public class DemoApplication {
 
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
