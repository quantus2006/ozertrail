package com.indirimania.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.indirimania.entities.PhpbbAttachment;
import com.indirimania.entities.PhpbbForum;
import com.indirimania.entities.PhpbbPost;
import com.indirimania.entities.PhpbbTopic;
import com.indirimania.entities.attachment.AttachmentService;
import com.indirimania.entities.forum.ForumService;
import com.indirimania.entities.post.PostService;
import com.indirimania.entities.topic.TopicService;

@Controller 
public class GreetingController {

	@Autowired TopicService service;
	@Autowired ForumService forumService;
	@Autowired PostService postService; 
	@Autowired AttachmentService attachmentService; 


	private static String UPLOADED_FOLDER = "C:\\Program Files\\Ampps\\www\\phpBB3\\files";
	//private static String UPLOADED_FOLDER = "/Applications/XAMPP/htdocs/phpBB3/files/";

	@RequestMapping(value = "/frontpage", method = RequestMethod.GET)
	public String greeting(Model model) {



		int sublimit;
		List<PhpbbTopic> listAll = service.listAll().subList(0, 50);

		if (listAll.size()<5) 
			sublimit=listAll.size();
		else sublimit=5;
		List<PhpbbTopic> rightPane =  listAll.subList(0, sublimit);

  
		model.addAttribute("topics", listAll);
		model.addAttribute("right", rightPane);



		return "im_main";
	}


	@RequestMapping(value = "/topicnu/{topicId}", method = RequestMethod.GET) // Clientten gelen requestin formatÄ± ve tipi
	public String getTopic(@PathVariable Integer topicId, Model model) 
	{


		Long time = System.currentTimeMillis();
		int sublimit;
		PhpbbTopic topic = service.get(topicId);
		topic.setTopicLastViewTime((int)(time/1000));
		topic.setTopicViews(topic.getTopicViews()+1);
		List<PhpbbTopic> listAll = service.listAll();


		service.save(topic); // görüntülenme bir artacak

		if (listAll.size()<5) 
			sublimit=listAll.size();
		else sublimit=5;


		List<PhpbbTopic> rightPane =  listAll.subList(0, sublimit);

		if (topic!=null) {

			model.addAttribute("topic",topic);
			model.addAttribute("right", rightPane);
			return "product";

		}


		else return "product";


	}




	@PostMapping("/postdeal")
	public String greetingSubmit(@ModelAttribute PhpbbTopic newTopic) {

		service.save(newTopic);

		return "result";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String postdealmain() {

		return "redirect:/alibabafrontpage";
	}

	@RequestMapping(value = "/alibaba", method = RequestMethod.GET)
	public String alibaba() {

		return "starter-alibaba-home";
	}



	@RequestMapping(value = "/alibabafrontpage", method = RequestMethod.GET)
	public String alibabafrontpage(Model model) {

		int limit = 50 ;
		int sublimit;
		List<PhpbbTopic> listAll = service.listAll();
		if (listAll.size()>50) limit = 50;
		else limit=listAll.size();

		listAll = service.listAll().subList(0, limit);
		if (listAll.size()<5) 
			sublimit=listAll.size();
		else sublimit=5;
		List<PhpbbTopic> rightPane =  listAll.subList(0, sublimit);

		model.addAttribute("topics", listAll);
		model.addAttribute("right", rightPane);
		return "starter-alibaba-home";
	}

	@RequestMapping(value = "/alibabapostdeal", method = RequestMethod.GET)
	public String alibabapostdeal(Model model) {
		List<PhpbbTopic> list = service.listAll();


		model.addAttribute("topics", list);
		return "starter-alibaba-postdeal";
	}

	@RequestMapping(value="/dealpost", method=RequestMethod.POST)
	public String postdeal(@Valid PhpbbTopic deal, BindingResult result,@RequestParam("file") MultipartFile file,@RequestParam("postText") String postText,
			@RequestParam("url") String url,
			RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "starter-alibaba-postdeal";
		}


		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			return "redirect:uploadStatus";
		}

		try {

			// Get the file and save it somewhere
			byte[] bytes = file.getBytes();
			Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
			Files.write(path, bytes);

			redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + file.getOriginalFilename() + "'");

		} catch (IOException e) {
			e.printStackTrace();
		}






		String fileName = file.getOriginalFilename();


		Long timestamp = System.currentTimeMillis();
		int time = (int) (timestamp /1000);
		PhpbbAttachment fileRecord = new PhpbbAttachment ();
		byte orphan =0;

		fileRecord.setFilesize((int)file.getSize());
		fileRecord.setFiletime(time);
		fileRecord.setIsOrphan(orphan);
		fileRecord.setPhysicalFilename(fileName);
		fileRecord.setRealFilename(fileName);
		System.out.println (file.getContentType());





		byte visiblity =1;


		deal.setTopicTitle(deal.getTopicTitle());
		deal.setPollTitle("");
		deal.setPollVoteChange("0");
		deal.setTopicBumped("0");
		deal.setTopicVisibility(visiblity);
		deal.setTopicReported("0");
		deal.setTopicPoster(2); // DUZENLENECEK USER OLACAK
		deal.setTopicTime(time);
		deal.setTopicLastViewTime(time);
		deal.setTopicLastPostTime(time);


		PhpbbPost post = new PhpbbPost();

		post.setPosterId(2); //DUZENLENECEK UZER OLACAK
		post.setPosterIp("127.0.0.1");
		post.setForumId(2);	// DÜZENLENECEK
		post.setPostText(postText);
		post.setPostVisibility(visiblity);
		post.setEnableBbcode(visiblity);
		post.setEnableSmilies(visiblity);
		post.setEnableMagicUrl(visiblity);
		post.setEnableSig(visiblity);
		post.setPostSubject(deal.getTopicTitle());
		post.setPostTime(time);


		PhpbbForum a = forumService.get(3);
		deal.setPhpbbForum(a);
		deal.setTopicFirstPosterColour("AA0000");
		deal.setTopicLastPosterColour("AA0000");


		deal.setTopicFirstPostId(post.getPostId()); /// süpheli
		deal.setTopicLastPostId(post.getPostId());/// süpheli
		deal.setTopicFirstPosterName("Admin"); //DUZENLENECEK UZER OLACAK
		deal.setTopicLastPosterName("Admin"); //DUZENLENECEK UZER OLACAK
		deal.setTopicLastPosterId(2);//DUZENLENECEK UZER OLACAK
		deal.setPollMaxOptions(visiblity);
		deal.setTopicPostsApproved(visiblity);
		deal.setTopicUrl(url);

		

		post.setPhpbbTopic(deal);
		post.setForumId(a.getForumId());




		post.setPostAttachment(visiblity);
		deal.setTopicAttachment(visiblity);


		if (deal.getPhpbbAttachments()==null) {

			List<PhpbbAttachment> attachments = new ArrayList<PhpbbAttachment>();
			deal.setPhpbbAttachments(attachments);	
		}

		deal.addPhpbbAttachment(fileRecord);












		service.save(deal);


		postService.save(post);




		fileRecord.setPhpbbTopic(deal);
		fileRecord.setPhpbbPost(post);
		fileRecord.setAttachComment(deal.getTopicTitle());
		fileRecord.setMimetype(file.getContentType());
		fileRecord.setExtension("jpg"); // DUZENLENECKE
		fileRecord.setThumbnail(orphan);

		attachmentService.save(fileRecord);


		return "redirect:/alibabafrontpage";


	}


	@RequestMapping(value="/commentpost", method=RequestMethod.POST)
	public String postcomment(@Valid PhpbbPost comment, BindingResult result,@RequestParam("productId") String productIdS,
			RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "starter-alibaba-postdeal";
		}

		Long timestamp = System.currentTimeMillis();
		int time = (int) (timestamp /1000);

		byte visiblity =1;
		int productId = Integer.valueOf(productIdS);
		PhpbbTopic topic = service.get(productId);

		comment.setPosterId(2); //DUZENLENECEK UZER OLACAK
		comment.setPosterIp("127.0.0.1");
		comment.setPostText(comment.getPostText());
		comment.setPhpbbTopic(topic);
		comment.setForumId(topic.getPhpbbForum().getForumId());
		comment.setPostVisibility(visiblity);
		comment.setEnableBbcode(visiblity);
		comment.setEnableSmilies(visiblity);
		comment.setEnableMagicUrl(visiblity);
		comment.setEnableSig(visiblity);
		comment.setPostSubject(topic.getTopicTitle());
		comment.setPostTime(time);
		

		topic.setTopicLastPostSubject(comment.getPostSubject());
		topic.addPhpbbPost(comment);
		topic.setTopicLastPosterId(2);//DUZENLENECEK UZER OLACAK
		topic.setTopicLastPosterColour("AA0000");
		topic.setTopicLastPosterName("Admin"); //DUZENLENECEK UZER OLACAK
		topic.setTopicLastPostTime(time);

		postService.save(comment);

		topic.setTopicLastPostId(comment.getPostId()); // Süpheli
		service.save(topic);


		return "redirect:/topicnu/"+topic.getTopicId();


	}

	@RequestMapping(value="/fillform", method=RequestMethod.POST)
	public String formfiller(@RequestParam("url") String url,RedirectAttributes redirectAttributes, Model model) {

		Document doc=null;
		String productName ="";
		String old_price ="";
		String new_price ="";
		
		String imageUrl ="";
		try {
			doc = Jsoup.connect(url).get();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		if (url.contains("hepsiburada")) {
			
			
			//description = doc.select("meta[property=og:title]").get(0).attr("content");
			
			
			imageUrl = doc.select("img[data-cloudzoom]").get(0).text();
			
			
			
			Element productNameElement = doc.getElementById("product-name");
			Element old_priceElement = doc.getElementById("originalPrice");
			Element new_priceElement = doc.getElementById("offering-price");
			
			productName =productNameElement.text();
			old_price=old_priceElement.text();
			
			
			new_price=new_priceElement.attr("content");
		
			
		
			
			
			
			
		}
		
	
		model.addAttribute("productTitle",productName);
		model.addAttribute("topicurl",url);
		model.addAttribute("imageUrl",imageUrl);
		model.addAttribute("old_price",old_price);
		model.addAttribute("new_price",new_price);


		//ModelAndView modelAndView =  new ModelAndView("redirect:/starter-alibaba-postdeal");
		//modelAndView.addObject("modelAttribute" , model);
		//return modelAndView;


		return "starter-alibaba-postdeal";
	}


}
