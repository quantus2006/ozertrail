package com.indirimania.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.indirimania.entities.PhpbbTopic;
import com.indirimania.entities.topic.TopicService;

@Controller
@RequestMapping("/image")
public class ImageController {

	@Autowired
	 TopicService topicService;

	@RequestMapping(value = "/imageDisplay", method = RequestMethod.GET)
	public void showImage(@RequestParam("id") int itemId, HttpServletResponse response,HttpServletRequest request) 
			throws ServletException, IOException{


		PhpbbTopic item = topicService.get(itemId);        
		response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
		response.getOutputStream().write(item.getPhpbbAttachments().get(0).getPhysicalFilename().getBytes());


		response.getOutputStream().close();
	}
}